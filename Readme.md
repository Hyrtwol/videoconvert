# Video Convert

Made this little msbuild script for converting overwatch videos and share them in [OneDrive](https://1drv.ms/f/s!AgZi8vvkMZx3gZ1VL_2vE7uiY9VRCA)

It uses ffmpeg for the conversion and must be installed separately.

The path used can be tweaked in ffmpeg.targets

Convert.recipe contains info about source video and cutout offset, all the videos have the same length because the Overwatch highlights are always ~20 sec.

To run the script use the following commands in a msbuild prompt

### Clean

	msbuild Convert.recipe /t:Clean

Cleanup temporary files e.g. ffmpeg logs.

### Build

	msbuild Convert.recipe /t:Build

Execute the video conversions for all the build items that are enabled.

### Publish

	msbuild Convert.recipe /t:Publish

Copy temporary output files to the OneDrive folder

### Full

	msbuild Convert.recipe /t:Clean;Build;Publish

Executes the 3 targets in one go.
